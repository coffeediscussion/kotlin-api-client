package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * The class represents the PNL model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ProfitAndLoss {
    @JsonProperty("accountName")
    var accountName: String? = null
    @JsonProperty("accountNo")
    var accountNo: Long = 0
    @JsonProperty("accountType")
    var accountType: String? = null
    @JsonProperty("balance")
    var balance: BigDecimal? = null
    @JsonProperty("subFundIdPnl")
    var subFundIdPnl: String? = null
    @JsonProperty("weightTNAV")
    var weightTNav: Double = 0.toDouble()

    override fun toString(): String =
            "ProfitAndLoss[ accountName: $accountName, accountNo: $accountNo, accountType: $accountType, " +
                    "balance: $balance, subFundIdPnl: $subFundIdPnl, weightTNav: $weightTNav ]"
}