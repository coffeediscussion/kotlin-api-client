package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonProperty

class Token(
        @JsonProperty("access_token")
        val accessToken: String,
        @JsonProperty("token_type")
        val tokenType: String = "Bearer",
        @JsonProperty("expires_in")
        val expiresIn: Int = 7199
)