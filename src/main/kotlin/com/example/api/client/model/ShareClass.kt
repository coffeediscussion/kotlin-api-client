package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * The class represents the View Share Pack model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ShareClass {
    @JsonProperty("changePercent")
    var changePercent: Double? = null
    @JsonProperty("differencePercent")
    var differencePercent: Double? = null
    @JsonProperty("distributionFactor")
    var distributionFactor: Double? = null
    @JsonProperty("distributionFactorGains")
    var distributionFactorGains: Double? = null
    @JsonProperty("distributionFactorIncome")
    var distributionFactorIncome: Double? = null
    @JsonProperty("nav")
    var nav: Double? = null
    @JsonProperty("outstandingShares")
    var outstandingShares: Double? = null
    @JsonProperty("shareClass")
    var shareClass: String? = null
    @JsonProperty("shareClassCurrency")
    var shareClassCurrency: String? = null
    @JsonProperty("shareClassId")
    var shareClassId: String? = null
    @JsonProperty("tNav")
    private var tNav: Double? = null
    @JsonProperty("totalInflow")
    var totalInflow: Double? = null
    @JsonProperty("totalOutflow")
    var totalOutflow: Double? = null

    override fun toString(): String =
            "${javaClass.simpleName}[ shareClassId: $shareClassId, shareClass: $shareClass, shareClassCurrency: $shareClassCurrency ]"
}