package com.example.api.client.model

import java.time.LocalDate

/**
 *
 * The class represents a set of attributes to filter the results.
 *
 * @param client a nullable list of client codes. The result set will contain data for the intersection of the
 * codes in the list with the codes owned by the user. For example, if the list contains "``123"``, ``"234"`` and the user
 * owns ``"234"``, ``"456"``, only the data with client code "234" will be returned. If the list is null then data with all
 * client codes owned by the user will be returned. If the list is empty no data will be returned (the intersection in
 * this case will be empty).
 *
 * @param portfolio a nullable list of portfolios/subfunds to return. The result set will contain data for the
 * intersection of the  portfolios is the list with the portfolios that the current user is permitted to access. If the
 * list is null all portfolios the user has access to will be returned. If the list is empty no data will be returned
 * (the intersection in this case will be empty).
 *
 * @param startDate the start date of the result set in the form of ``yyyymmdd`` string. If the start date is not set
 * (``null``) then yesterday will be taken as a start date (*not last business day!*)
 *
 * @param endDate the end date (*inclusive*) of the result set in the form of ``yyyymmdd`` string. By default, if the
 * end date is not set (``null``) it will be set to the start date. *Please note that the max date range is limited by
 * 31 days!*
 *
 * @param exclude a list of fields to exclude from the result set. If the list is ``null`` or empty all the fields
 * will be returned.
 */
data class Filter(
        val client: List<String>? = null,
        val portfolio: List<String>? = null,
        val startDate: LocalDate? = null,
        val endDate: LocalDate? = null,
        val exclude: List<String>? = null
)