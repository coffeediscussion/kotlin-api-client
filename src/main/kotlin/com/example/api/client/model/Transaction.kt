package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * The class represents the Transaction model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Transaction {
    @JsonProperty("accruedInterestPositionCurrency")
    var accruedInterestPositionCurrency: Double? = null
    @JsonProperty("accruedInterestSubFundCurrency")
    var accruedInterestSubFundCurrency: Double? = null
    @JsonProperty("adjustmentType")
    var adjustmentType: String? = null
    @JsonProperty("baseProceeds")
    var baseProceeds: Double? = null
    @JsonProperty("bloombergId")
    var bloombergId: String? = null
    @JsonProperty("bookValuePositionCurrency")
    var bookValuePositionCurrency: Double? = null
    @JsonProperty("class2Name")
    var class2Name: String? = null
    @JsonProperty("class3Name")
    var class3Name: String? = null
    @JsonProperty("counterPartyId")
    var counterPartyId: String? = null
    @JsonProperty("counterPartyName")
    var counterPartyName: String? = null
    @JsonProperty("counterPartyShortName")
    var counterPartyShortName: String? = null
    @JsonProperty("cusip")
    var cusip: String? = null
    @JsonProperty("divIncomeBase")
    var divIncomeBase: Double? = null
    @JsonProperty("divIncomeLocal")
    var divIncomeLocal: Double? = null
    @JsonProperty("exRate")
    var exRate: Double? = null
    @JsonProperty("expiryDate")
    var expiryDate: String? = null
    @JsonProperty("fundName")
    var fundName: String? = null
    @JsonProperty("internalPositionId")
    var internalPositionId: String? = null
    @JsonProperty("localProceeds")
    var localProceeds: Double? = null
    @JsonProperty("marketPrice")
    var marketPrice: Double? = null
    @JsonProperty("netDividendLocal")
    var netDividendLocal: Double? = null
    @JsonProperty("openCloseIndicator")
    var openCloseIndicator: String? = null
    @JsonProperty("positionCurrency")
    var positionCurrency: String? = null
    @JsonProperty("positionName")
    var positionName: String? = null
    @JsonProperty("positionType")
    var positionType: String? = null
    @JsonProperty("quantity")
    var quantity: Double? = null
    @JsonProperty("realizedResultSubFundCurrency")
    var realizedResultSubFundCurrency: Double? = null
    @JsonProperty("reutersId")
    var reutersId: String? = null
    @JsonProperty("sedol")
    var sedol: String? = null
    @JsonProperty("settleAmountPositionCurrency")
    var settleAmountPositionCurrency: Double? = null
    @JsonProperty("settleAmountSubFundCurrency")
    var settleAmountSubFundCurrency: Double? = null
    @JsonProperty("settleCurrency")
    var settleCurrency: String? = null
    @JsonProperty("sourceAccountNumber")
    var sourceAccountNumber: String? = null
    @JsonProperty("subFundIdTransaction")
    var subFundIdTransaction: String? = null
    @JsonProperty("tradeBrokerFees")
    var tradeBrokerFees: Double? = null
    @JsonProperty("tradeCommissions")
    var tradeCommissions: Double? = null
    @JsonProperty("tradeDate")
    var tradeDate: String? = null
    @JsonProperty("transactionId")
    var transactionId: String? = null
    @JsonProperty("transactionType")
    var transactionType: String? = null
    @JsonProperty("valueDate")
    var valueDate: String? = null
    @JsonProperty("weightRealizedResultSubFundTNav")
    var weightRealizedResultSubFundTNav: Double? = null

    override fun toString(): String =
            "Transaction[ transactionId: $transactionId, marketPrice: $marketPrice, quantity: $quantity, " +
                    "positionCurrency: $positionCurrency, transactionType: $transactionType ]"
}