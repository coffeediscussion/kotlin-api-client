package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * The class represents the Portfolio model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Portfolio {
    @JsonProperty("aggregateBalanceSheetPacks")
    var aggregateBalanceSheetPacks: List<BalanceSheet>? = null
    @JsonProperty("aggregatePositionPacks")
    var aggregatePositionPacks: List<Position>? = null
    @JsonProperty("aggregateProfitAndLossPacks")
    var aggregateProfitAndLossPacks: List<ProfitAndLoss>? = null
    @JsonProperty("aggregateTransactionPacks")
    var aggregateTransactionPacks: List<Transaction>? = null
    @JsonProperty("changePercentTNav")
    var changePercentTNav: BigDecimal? = null
    @JsonProperty("clientId")
    var clientId: String? = null
    @JsonProperty("_id")
    var id: String? = null
    @JsonProperty("lastUpdated")
    var lastUpdated: String? = null
    @JsonProperty("navDate")
    var navDate: String? = null
    @JsonProperty("payBalSheetPercentToNetNav")
    var payBalSheetPercentToNetNav: BigDecimal? = null
    @JsonProperty("payableBalanceSheet")
    var payableBalanceSheet: BigDecimal? = null
    @JsonProperty("recBalSheetPercentToNetNav")
    var recBalSheetPercentToNetNav: BigDecimal? = null
    @JsonProperty("receivableBalanceSheet")
    var receivableBalanceSheet: BigDecimal? = null
    @JsonProperty("shareClasses")
    var shareClasses: List<ShareClass>? = null
    @JsonProperty("subFundCurrency")
    var subFundCurrency: String? = null
    @JsonProperty("subFundId")
    var subFundId: String? = null
    @JsonProperty("subFundName")
    var subFundName: String? = null
    @JsonProperty("tNav")
    private var tNav: BigDecimal? = null

    override fun toString(): String =
            "Portfolio[ id: $id, clientId: $clientId, subFundId: $subFundId, subFundName: $subFundName, " +
                    "subFundCurrency: $subFundCurrency ]"
}