package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * The class represents the Balance Sheet model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class BalanceSheet {
    @JsonProperty("accountName")
    var accountName: String? = null
    @JsonProperty("accountNo")
    var accountNo: String? = null
    @JsonProperty("accountType")
    var accountType: String? = null
    @JsonProperty("balance")
    var balance: BigDecimal? = null
    @JsonProperty("subFundIdBalanceSheet")
    var subFundIdBalanceSheet: String? = null
    @JsonProperty("weightTNav")
    var weightTNav: Double = 0.toDouble()

    override fun toString(): String =
            "BalanceSheet[ accountName: $accountName, accountNo: $accountNo, accountType: $accountType, " +
                    "balance: $balance, subFundIdBalanceSheet: $subFundIdBalanceSheet, " +
                    "weightTNav: $weightTNav ]"
}