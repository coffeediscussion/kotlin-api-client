package com.example.api.client.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * The class represents the Position model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Position {
    @JsonProperty("accrInterestsPositionCurrency")
    var accrInterestsPositionCurrency: Double? = null
    @JsonProperty("accrInterestsSubFundCurrency")
    var accrInterestsSubFundCurrency: Double? = null
    @JsonProperty("baseGainLoss")
    var baseGainLoss: Double? = null
    @JsonProperty("bookValue")
    var bookValue: Double? = null
    @JsonProperty("bookValuePositionCurrency")
    var bookValuePositionCurrency: Double? = null
    @JsonProperty("bookValueSubFundCurrency")
    var bookValueSubFundCurrency: Double? = null
    @JsonProperty("brokerLongName")
    var brokerLongName: String? = null
    @JsonProperty("class2Name")
    var class2Name: String? = null
    @JsonProperty("class3Name")
    var class3Name: String? = null
    @JsonProperty("commission")
    var commission: Double? = null
    @JsonProperty("cusip")
    var cusip: String? = null
    @JsonProperty("discountRate")
    var discountRate: Double? = null
    @JsonProperty("exchangeRate")
    var exchangeRate: Double = 0.toDouble()
    @JsonProperty("expiryDate")
    var expiryDate: String? = null
    @JsonProperty("fee")
    var fee: Double? = null
    @JsonProperty("fwdLongAmount")
    var fwdLongAmount: Double? = null
    @JsonProperty("fwdLongCurrency")
    var fwdLongCurrency: String? = null
    @JsonProperty("fwdShortAmount")
    var fwdShortAmount: Double? = null
    @JsonProperty("fwdShortCurrency")
    var fwdShortCurrency: String? = null
    @JsonProperty("interestRate")
    var interestRate: Double? = null
    @JsonProperty("internalPositionId")
    var internalPositionId: String? = null
    @JsonProperty("issueCountry")
    var issueCountry: String? = null
    @JsonProperty("localGainLoss")
    var localGainLoss: Double? = null
    @JsonProperty("marketPrice")
    var marketPrice: BigDecimal? = null
    @JsonProperty("marketValuePositionCurrency")
    var marketValuePositionCurrency: BigDecimal? = null
    @JsonProperty("marketValueSubFundCurrency")
    var marketValueSubFundCurrency: BigDecimal? = null
    @JsonProperty("maturityDate")
    var maturityDate: String? = null
    @JsonProperty("positionCurrency")
    var positionCurrency: String? = null
    @JsonProperty("positionName")
    var positionName: String? = null
    @JsonProperty("positionQuantity")
    var positionQuantity: BigDecimal? = null
    @JsonProperty("positionType")
    var positionType: String? = null
    @JsonProperty("realized")
    var realized: Double? = null
    @JsonProperty("securityBic")
    var securityBic: String? = null
    @JsonProperty("securityRic")
    var securityRic: String? = null
    @JsonProperty("securitySubType")
    var securitySubType: String? = null
    @JsonProperty("sedol")
    var sedol: String? = null
    @JsonProperty("settlementDate")
    var settlementDate: String? = null
    @JsonProperty("sourceAccountName")
    var sourceAccountName: String? = null
    @JsonProperty("status")
    var status: String? = null
    @JsonProperty("strikeRate")
    var strikeRate: Double? = null
    @JsonProperty("subFundIdPosition")
    var subFundIdPosition: String? = null
    @JsonProperty("swapId")
    var swapId: String? = null
    @JsonProperty("tradeDate")
    var tradeDate: String? = null
    @JsonProperty("unrealizedGainLossPercent")
    var unrealizedGainLossPercent: Double? = null
    @JsonProperty("unrealizedGainLossPositionCurrency")
    var unrealizedGainLossPositionCurrency: Double? = null
    @JsonProperty("unrealizedGainLossSubFundCurrency")
    var unrealizedGainLossSubFundCurrency: Double? = null
    @JsonProperty("weightTNav")
    var weightTNav: Double = 0.toDouble()

    override fun toString(): String =
            "Position[ positionName: $positionName, marketPrice: $marketPrice, " +
                    "positionQuantity: $positionQuantity, positionCurrency: $positionCurrency, " +
                    "positionType: $positionType, cusip: $cusip, sedol: $sedol ]"
}