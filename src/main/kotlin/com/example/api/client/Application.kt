package com.example.api.client

import com.example.api.client.services.FundAccountingService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application(val service: FundAccountingService) : CommandLineRunner {

    override fun run(vararg args: String?) {
        // List all records for the given date
        val portfolios = service
                .list(size = 10)
        portfolios.forEach(::println)

        portfolios.firstOrNull()?.apply {

            service.findById(id!!).apply { println(this) }

            // List of balance sheets for the given record
            service.listBalanceSheets(id!!).forEach(::println)

            // List of PNLs for the given record
            service.listProfitAndLosses(id!!).forEach(::println)

            // List of positions for the given record
            service.listPositions(id!!, listOf("positionName", "sedol")).forEach(::println)

            service.listShareClasses(id!!).forEach(::println)

            // List of transactions for the given record
            service.listTransactions(id!!).forEach(::println)
        }
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
