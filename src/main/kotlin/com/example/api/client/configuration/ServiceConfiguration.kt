package com.example.api.client.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI

/**
 *
 * @author Eugene Ossipov
 */
open class EndpointConfiguration {
    /**
     * URL scheme.
     */
    var scheme: String = "https"
    /**
     * Service host provided by the RBC One Data API team. In the test app we use QA environment.
     * Please contact the RBC One Data API team for production environment information.
     */
    var host: String = "localhost"
    /**
     * Root path for all endpoints
     */
    var rootPath: String = ""

    protected open fun values(): String = "scheme=$scheme,host=$host,rootPath=$rootPath"

    override fun toString(): String = "${javaClass.simpleName}{ ${values()} }"

    fun url(apply: (builder: UriComponentsBuilder) -> Unit): URI =
            UriComponentsBuilder.newInstance()
                    .scheme(scheme).host(host).path(rootPath)
                    .apply(apply).build(true).toUri()

    fun url(): URI =
            UriComponentsBuilder.newInstance()
                    .scheme(scheme).host(host).path(rootPath)
                    .build(true).toUri()
}

@Configuration
@ConfigurationProperties(prefix = "rbcone.api")
@Component
class ApiEndpointConfiguration : EndpointConfiguration()

@Configuration
@ConfigurationProperties(prefix = "rbcone.auth")
@Component
class AuthEndpointConfiguration
    : EndpointConfiguration() {

    /**
     * The client/app ID provided by the RBC One Data API team.
     */
    lateinit var clientId: String
    /**
     * The client secret provided by the RBC One Data API team.
     */
    lateinit var clientSecret: String

    override fun values(): String = super.values() + ",clientId=$clientId"
}

@Configuration
class ServiceConfiguration {

    @Bean
    fun objectMapper(): ObjectMapper = ObjectMapper()
}
