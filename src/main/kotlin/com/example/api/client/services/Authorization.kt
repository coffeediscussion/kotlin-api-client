package com.example.api.client.services

import com.example.api.client.configuration.AuthEndpointConfiguration
import com.example.api.client.model.Token
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.POST
import org.springframework.http.MediaType
import org.springframework.http.MediaType.*
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

val TEXT_PLAIN_UTF8 = MediaType.valueOf("text/plain;charset=UTF-8")

/**
 *
 * @author Eugene Ossipov
 * A sample of Authorization service implementation. The token is retrieved on a first call. *Please note:* The service
 * does not implement the renewal of the JWT.
 */
@Service
class Authorization(
        private val authEndpointConfiguration: AuthEndpointConfiguration,
        private val objectMapper: ObjectMapper,
        restTemplateBuilder: RestTemplateBuilder
) {

    private val logger = LoggerFactory.getLogger(Authorization::class.java)

    private val restTemplate: RestTemplate = restTemplateBuilder.build()

    val token: Token by lazy { authorize() }

    /**
     *
     * The method implements the process of authorization of the client app/service. Please note that the Client ID
     * and Client Secret must be passed as application/x-www-form-urlencoded content.
     */
    internal fun authorize(): Token {
        val headers = HttpHeaders()
                .apply {
                    accept = listOf(APPLICATION_JSON)
                    contentType = APPLICATION_FORM_URLENCODED
                }

        val values = LinkedMultiValueMap<String, String>()
                .apply {
                    add("client_id", authEndpointConfiguration.clientId)
                    add("client_secret", authEndpointConfiguration.clientSecret)
                    add("grant_type", "client_credentials")
                    add("scope", "read")
                }

        val entity = RequestEntity<MultiValueMap<String, String>>(values, headers, POST, authEndpointConfiguration.url())
        val response = restTemplate.exchange(entity, String::class.java)
        checkResponse(entity, response)

        return when (response.headers.contentType) {
            APPLICATION_JSON, APPLICATION_JSON_UTF8 -> objectMapper.readValue(response.body, Token::class.java)
            TEXT_PLAIN, TEXT_PLAIN_UTF8 -> response.body?.split(" ")?.takeIf { it.size == 2 }
                    ?.let { Token(accessToken = it[1]) }
                    ?: throw RuntimeException("Invalid token format: ${response.body}")
            else -> throw RuntimeException("Unsupported media type: ${response.headers.contentType}")
        }
    }

    private fun <T, R> checkResponse(entity: RequestEntity<T>, response: ResponseEntity<R>) {
        if (!response.statusCode.is2xxSuccessful) {
            logger.error("Url: ${entity.url}, Method: ${entity.method}, Status: ${response.statusCode}")
            throw HttpClientErrorException(response.statusCode)
        }
    }
}