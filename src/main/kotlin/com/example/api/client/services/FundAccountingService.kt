package com.example.api.client.services

import com.example.api.client.configuration.ApiEndpointConfiguration
import com.example.api.client.model.*
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import java.time.format.DateTimeFormatter.BASIC_ISO_DATE

private const val DEFAULT_PAGE = 0

private const val DEFAULT_PAGE_SIZE = 25

/**
 *
 * The class is a sample of domain-specific service implementation. This class represents Fund Accounting service.
 * Please note that the class is for demo-purposes only and may be incomplete in terms of business functionality.
 * The purpose of the class is to demo how the client's application/service can interact with the Data REST API.
 * For full list of supported endpoints please refer to the Swagger web page.
 *
 * All Fund Accounting service methods that return a list of items support pagination and date range.
 * Even though the page and size parameters are optional on the Data REST API side we highly encourage you to
 * make them mandatory on the client side. Considering the high volume of data, not specifying the parameters
 * may cause interruptions and service quality degradation during the data transfer between the servers and
 * clients.
 *
 * @see com.example.api.client.services.BaseService
 */
@Service
class FundAccountingService(authorization: Authorization,
                            apiEndpointConfiguration: ApiEndpointConfiguration,
                            restTemplateBuilder: RestTemplateBuilder)
    : BaseService(authorization, apiEndpointConfiguration, restTemplateBuilder) {

    /**
     * Returns all details for the given id. If the record is not found the service will throw 404 error. This method
     * should be used if all details are required. If only specific subcollections are required then the specialized
     * methods are more preferable.
     *
     * @param id an id of a specific record. To discover IDs for a given set of records please refer to [list] method.
     */
    fun findById(id: String): Portfolio =
            get(Portfolio::class.java) {
                it.pathSegment("fa", "v2", "portfolios", id)
            }

    /**
     * Returns a summary list of portfolios limited by the filter and pagination.
     *
     * @param filter query parameters to limit the result set. See [Filter] for more details.
     *
     * @param page a page of the result set to return.
     *
     * @param size a size of the page.
     */
    fun list(filter: Filter = Filter(), page: Int = DEFAULT_PAGE, size: Int = DEFAULT_PAGE_SIZE): List<Portfolio> {
        val typeReference = object : ParameterizedTypeReference<List<Portfolio>>() {

        }
        return list(typeReference) { builder ->
            builder.pathSegment("fa", "v2", "portfolios")
                    .queryParam("page", page)
                    .queryParam("size", size)
            with(filter) {
                client?.forEach { builder.queryParam("client", it) }
                portfolio?.forEach { builder.queryParam("portfolio", it) }
                exclude?.forEach { builder.queryParam("exclude", it) }
                startDate?.apply { builder.queryParam("startDate", this.format(BASIC_ISO_DATE)) }
                endDate?.apply { builder.queryParam("endDate", this.format(BASIC_ISO_DATE)) }
            }
        }
    }

    fun listBalanceSheets(id: String, exclude: List<String> = emptyList()): List<BalanceSheet> {
        val typeReference = object : ParameterizedTypeReference<List<BalanceSheet>>() {

        }
        return listEntityById(id, "balancesheets", exclude, typeReference)
    }

    /**
     * A generic method that retrieves portfolio-specific details such as balance sheets, positions, transactions,
     * etc. It requests items from the Data REST API, converts them from JSON to a domain class and returns a list of the
     * converted items.
     *
     * *Please refer to the Swagger web page for full list of supported endpoints.*
     *
     * @param id an id of a specific record
     * @param entity a details-specific portion of the endpoint (such as "transactions")
     * @param typeReference the type of the domain class
     * @return a list of the class instances
     */
    private fun <T> listEntityById(id: String, entity: String, exclude: List<String>,
                                   typeReference: ParameterizedTypeReference<List<T>>): List<T> =
            list(typeReference) { builder ->
                builder.pathSegment("fa", "v2", "portfolios", id, entity)
                exclude.forEach { builder.queryParam("exclude", it) }
            }

    fun listPositions(id: String, exclude: List<String> = emptyList()): List<Position> {
        val typeReference = object : ParameterizedTypeReference<List<Position>>() {

        }
        return listEntityById(id, "positions", exclude, typeReference)
    }

    fun listProfitAndLosses(id: String, exclude: List<String> = emptyList()): List<ProfitAndLoss> {
        val typeReference = object : ParameterizedTypeReference<List<ProfitAndLoss>>() {

        }
        return listEntityById(id, "profitandlosses", exclude, typeReference)
    }

    fun listShareClasses(id: String, exclude: List<String> = emptyList()): List<ShareClass> {
        val typeReference = object : ParameterizedTypeReference<List<ShareClass>>() {

        }
        return listEntityById(id, "shareclasses", exclude, typeReference)
    }

    fun listTransactions(id: String, exclude: List<String> = emptyList()): List<Transaction> {
        val typeReference = object : ParameterizedTypeReference<List<Transaction>>() {

        }
        return listEntityById(id, "transactions", exclude, typeReference)
    }
}