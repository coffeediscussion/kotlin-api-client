package com.example.api.client.services

import com.example.api.client.configuration.ApiEndpointConfiguration
import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.GET
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

/**
 * The class represents one of the approaches in implementation of the interaction with the Data REST API.
 * One of the key points here is the authentication (and authorization) process. Before any requests to the Data
 * REST API the client application (service) must receive JWT token from the authentication/authorization service.
 * This token must be provided with each and every request to the rest of the services (i.e. Fund Accounting service).
 * The authentication process requires Client ID (aka App ID, Application ID) and Client Secret. The credentials
 * are provided by RBC.
 *
 * The Data REST API are available for the clients in two environments: QA and PROD. The QA environment should be
 * used for test and demo purposes. The PROD environment URL is provided by RBC for each client separately.
 */
open class BaseService(
        private val authorization: Authorization,
        private val apiEndpointConfiguration: ApiEndpointConfiguration,
        restTemplateBuilder: RestTemplateBuilder
) {

    private val logger = LoggerFactory.getLogger(BaseService::class.java)

    private val restTemplate: RestTemplate = restTemplateBuilder.build()

    private val token: String
        get() = authorization.token.run { "$tokenType $accessToken" }

    private fun <T, R> checkResponse(entity: RequestEntity<T>, response: ResponseEntity<R>): R {
        if (response.statusCode.is2xxSuccessful) {
            return response.body!!
        }
        logger.error("Url: ${entity.url}, Method: ${entity.method}, Status: ${response.statusCode}")
        throw HttpClientErrorException(response.statusCode)
    }

    /**
     * A utility method to send a GET request to the Data REST API. It builds an endpoint based on the ROOT_PATH,
     * service path and query string. It also adds default headers to the request, including the acceptable
     * media type and authorization header value (which is the JWT token received earlier).
     *
     * *Please note that the current demo accepts JSON only. However the Data REST API services may provide data
     * in other formats as well. Please refer to the Swagger web page for more details.*
     */
    fun <R> get(responseType: Class<R>, apply: (builder: UriComponentsBuilder) -> Unit): R {
        val headers = HttpHeaders()
                .apply {
                    accept = listOf(APPLICATION_JSON)
                    set("Authorization", token)
                }

        val entity = RequestEntity<Any>(headers, GET, apiEndpointConfiguration.url(apply))
        return sendMessage(entity, responseType)
    }

    fun <R> list(responseType: ParameterizedTypeReference<R>, apply: (builder: UriComponentsBuilder) -> Unit): R {
        val headers = HttpHeaders()
                .apply {
                    accept = listOf(APPLICATION_JSON)
                    set("Authorization", token)
                }

        val entity = RequestEntity<Any>(headers, GET, apiEndpointConfiguration.url(apply))
        return sendMessage(entity, responseType)
    }

    /**
     * A utility method that sends an HTTP request, checks the status of the response
     * and returns content of the response, if the response is successful; otherwise it throws an HTTP-specific
     * exception.
     */
    private fun <T, R> sendMessage(entity: RequestEntity<T>, responseType: Class<R>): R =
            checkResponse(entity, restTemplate.exchange(entity, responseType))

    private fun <T, R> sendMessage(entity: RequestEntity<T>, responseType: ParameterizedTypeReference<R>): R =
            checkResponse(entity, restTemplate.exchange(entity, responseType))
}