package com.example.api.client.services

import com.example.api.client.configuration.AuthEndpointConfiguration
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.FilterType
import org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest
import org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess
import org.springframework.web.client.HttpClientErrorException

/**
 * @author Eugene Ossipov
 */
@Configuration
@EnableConfigurationProperties
@ComponentScan(includeFilters = [
    ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = [Authorization::class])
], basePackages = ["com.example.api.client.services"], useDefaultFilters = false)
class AuthorizationTestConfiguration

@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest(classes = [AuthorizationTestConfiguration::class, AuthEndpointConfiguration::class])
@RestClientTest(Authorization::class)
class AuthorizationTest {

    @Autowired
    lateinit var authorization: Authorization

    @Autowired
    lateinit var server: MockRestServiceServer

    @Test
    fun getToken_json() {

        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
                .andRespond(withSuccess(
                        """
                            {"access_token":"eyJhbGciOiJSU","token_type":"Bear","expires_in":7200}
                        """.trimIndent(), APPLICATION_JSON_UTF8))

        val token = authorization.authorize()
        assertEquals("eyJhbGciOiJSU", token.accessToken)
        assertEquals("Bear", token.tokenType)
        assertEquals(7200, token.expiresIn)
    }

    @Test
    fun getToken_plain() {

        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
                .andRespond(withSuccess("Bear eyJhbGciOiJSU_XYZ", TEXT_PLAIN_UTF8))

        val token = authorization.authorize()
        assertEquals("eyJhbGciOiJSU_XYZ", token.accessToken)
        assertEquals("Bearer", token.tokenType)
        assertEquals(7199, token.expiresIn)
    }

    @Test(expected = RuntimeException::class)
    fun getToken_plain_invalid() {

        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
                .andRespond(withSuccess("eyJhbGciOiJSU_XYZ", TEXT_PLAIN_UTF8))

        authorization.authorize()
        fail()
    }

    @Test(expected = RuntimeException::class)
    fun getToken_invalid_media() {

        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
                .andRespond(withSuccess("Bear eyJhbGciOiJSU_XYZ", APPLICATION_FORM_URLENCODED))

        authorization.authorize()
        fail()
    }

    @Test(expected = HttpClientErrorException::class)
    fun getToken_http_error() {

        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
                .andRespond(withBadRequest())

        authorization.authorize()
        fail()
    }
}
