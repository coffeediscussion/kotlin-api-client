package com.example.api.client.services

import com.example.api.client.configuration.ApiEndpointConfiguration
import com.example.api.client.configuration.AuthEndpointConfiguration
import com.example.api.client.model.Portfolio
import com.example.api.client.model.Token
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

/**
 * @author Eugene Ossipov
 */
@Configuration
@EnableConfigurationProperties
@ComponentScan(basePackages = ["com.example.api.client.services"])
class BaseServiceTestConfiguration

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [BaseServiceTestConfiguration::class, ApiEndpointConfiguration::class, AuthEndpointConfiguration::class])
@RestClientTest(FundAccountingService::class)
class BaseServiceTest {

    @MockBean
    private lateinit var authorization: Authorization

    @Autowired
    private lateinit var service: FundAccountingService

    @Autowired
    lateinit var server: MockRestServiceServer

    @Before
    fun setUp() {
        given(authorization.token).willReturn(Token(accessToken = "XYZ"))
    }

    @Test
    fun get() {
        val portfolio = Portfolio().apply {
            clientId = "CI"
        }

        server.expect(requestTo("https://localhost/fa/v2/portfolios?page=0&size=25"))
                .andRespond(withSuccess(ObjectMapper().writeValueAsString(listOf(portfolio)), APPLICATION_JSON_UTF8))

        val portfolios = service.list()
        assertNotNull(portfolios)
        assertEquals(1, portfolios.size)
    }
}