package com.example.api.client.configuration

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

/**
 * @author Eugene Ossipov
 */
@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest(classes = [AuthEndpointConfiguration::class])
@EnableConfigurationProperties
class AuthEndpointConfigurationTest {

    @Autowired
    private lateinit var authEndpointConfiguration: AuthEndpointConfiguration

    @Test
    fun testConfiguration() {
        assertEquals("https", authEndpointConfiguration.scheme)
        assertEquals("ssoa.sterbc.com", authEndpointConfiguration.host)
        assertEquals("/as/token.oauth2", authEndpointConfiguration.rootPath)
        assertEquals("API-DEMO-CLIENT", authEndpointConfiguration.clientId)
        assertEquals("a84330347f837ca83f8515bc920d7a6c8f79f81f", authEndpointConfiguration.clientSecret)
    }
}