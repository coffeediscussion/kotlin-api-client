package com.example.api.client.configuration

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * @author Eugene Ossipov
 */
class EndpointConfigurationTest {

    @Test
    fun url() {
        val url = EndpointConfiguration().apply {
            host = "qa-api-rbcone.sterbc.com"
            rootPath = "/apiproxy/auth"
        }.url()
        assertEquals("https://qa-api-rbcone.sterbc.com/apiproxy/auth", url.toString())
    }

    @Test
    fun url_path() {
        val url = EndpointConfiguration().apply {
            host = "qa-api-rbcone.sterbc.com"
            rootPath = "/apiproxy"
        }.url { it.pathSegment("auth") }
        assertEquals("https://qa-api-rbcone.sterbc.com/apiproxy/auth", url.toString())
    }

    @Test
    fun url_query() {
        val url = EndpointConfiguration().apply {
            host = "qa-api-rbcone.sterbc.com"
            rootPath = "/apiproxy"
        }.url {
            it.pathSegment("auth")
                    .queryParam("page", 1)
                    .queryParam("size", 25)
        }
        assertEquals("https://qa-api-rbcone.sterbc.com/apiproxy/auth?page=1&size=25", url.toString())
    }
}