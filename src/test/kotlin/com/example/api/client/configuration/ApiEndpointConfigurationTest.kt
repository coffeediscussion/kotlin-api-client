package com.example.api.client.configuration

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

/**
 * @author Eugene Ossipov
 */
@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest(classes = [ApiEndpointConfiguration::class])
@EnableConfigurationProperties
class ApiEndpointConfigurationTest {

    @Autowired
    private lateinit var apiEndpointConfiguration: ApiEndpointConfiguration

    @Test
    fun testConfiguration() {
        assertEquals("https", apiEndpointConfiguration.scheme)
        assertEquals("qa-api-rbcone.sterbc.com", apiEndpointConfiguration.host)
        assertEquals("/apiproxy", apiEndpointConfiguration.rootPath)
    }
}